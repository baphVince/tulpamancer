#-*- coding: utf-8 -*-         
import sys, tweepy, yaml, urllib, re, time, random
with open('config.yaml') as f: config = yaml.load(f) # Get the config
auth = tweepy.OAuthHandler(config["api"]["ckey"], config["api"]["csecret"])
auth.set_access_token(config["api"]["atoken"], config["api"]["asecret"]) # Auth to the API
api = tweepy.API(auth) # Get an api object

def tulptweet(t):
	dText = t.text.encode('ascii','ignore')
	newtweet = re.sub(r"my \S*", "my tulpa", dText, 1, flags=re.I)
	newtweet = re.sub(r"@\S*", "", newtweet, 1, flags=re.I)
	if "tulpa" not in newtweet: # if we didn't replace anything somehow
		raise ValueError("Could not tulpify string!")
	print "[S] Tweeting {}".format(newtweet)
	try:
		api.update_status(status=newtweet)
	except: 
		print "[!] Tweet failed:"
		print traceback.format_exc()
		raise IOError("Could not tweet.")

def filterTweet(t):
	if t.source not in config["filter"]["sources"]: # if the source is not in a whitelist of sources
		return 0                                    # then drop the tweet
	if t.coordinates is not None:                      # if the tweet has coords
		if t.coordinates['coordinates'] == [0.0, 0.0]: # and coords are 0, 0
			return 0                                   # then drop the tweet     
	try:
		dText = t.text.encode('ascii','ignore') # Encode the tweet.
		if any(x in dText for x in config["filter"]["text"]): # If any tweet matches the filters
			return 0                          				  # drop the tweet
		try: 
			print "[T] @{0.author.screen_name}: '{1}' (source: {0.source})".format(t, dText) 
		except: 
			print "[!] Dark Lord Codethulu was almost summoned."
			return 0 # try not to summon the dark lord codethulu
							  # print the tweet
		return 1              # then pass the tweet on
	except Exception, e: # If an exception occurs
		raise
if __name__ == "__main__":
	searched_tweets = []
	last_id = -1
	while True: # go forever
		try:
			new_tweets = api.search(q=" my ", count=1000, max_id=str(last_id - 1)) # search twitter
			if not new_tweets:  # if there are no new tweets
				print "[z] No new tweets, sleeping for a second..."
				time.sleep(1)   # sleep a second
				continue        # reloop
			else: print "[M] New tweets!"
			if 1: #random.randint(0, 10) == 5:                                # if a d20 rolls a 5
				while True:												  # until we hit a break statement
					randtweet = random.choice(new_tweets)				  # pick a random tweet
					if filterTweet(randtweet):                            # if the tweet goes through
						try:
							tulptweet(randtweet)
						except ValueError, e:
							print "[!] ValueError: {}".format(e)
							print traceback.format_exc()
							print "[!] This just happens sometimes, pretend like it didn't happen and move on."
							continue
						except IOError, e:
							print "[!] Continuing..."
							continue
						except Exception, e: 
							print "[!] Some exception: {}".format(e)
							print traceback.format_exc()
							print "[!] Might be bad, so assuming it is and dying."
							exit(1)
						stime = random.uniform(0,10)
						print "[z] Sleeping for {0:.2f} seconds.".format(stime)
						time.sleep(stime)
						print "[z] Done sleeping, breaking..."
						break
					else: 
						print "[D] Picked a filtered tweet, rerolling..."
			last_id = new_tweets[-1].id
		except tweepy.TweepError as e:
			raise
		except KeyboardInterrupt:
			exit(0)
